# YF-S201 Linux kernel driver

#### This is kernel driver for Hall effect based water flow meters available on e-bay 
Module was tested on Arietta G25 board and 4.4.4 kernel.
```
spock@arietta:~$ cat /sys/flowsensor/eheim/currentFlow
168 L/h
spock@arietta:~$ cat /sys/flowsensor/eheim/totalLiters
43
spock@arietta:~$
```
### How to build
1. Edit Makefile and set KVER variable so it points to your kernel source used to build your running kernel.
2. run `make`
3. Copy flowsensor.ko to your destination board /lib/modules/$(uname -r)/extra folder
4. run `depmod -a`
5. Load module `modprobe <options>` i.e: `modprobe gpioSensor=67:eheim`

### Module options

- sensorEdge: Rising edge = 0 (default), Falling edge = 1 (bool)
- gpioSensor: GPIO PIN where pulse pin is connected(default=67) and name ex. 67:sensor1 (charp)
- samplingSize: Number of samples to calculate average (int)


