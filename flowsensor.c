/**
 * @file   flowsensor.c
 * @author Marcin Jurczuk
 * @date   6 march 2016
 * @brief  A kernel module for monitoring chinese hall effect based water flow sensors
 * Tested on YF-201 Sensor and Arietta G25 ARM board
*/

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/gpio.h>       // Required for the GPIO functions
#include <linux/interrupt.h>  // Required for the IRQ code
#include <linux/kobject.h>    // Using kobjects for the sysfs bindings
#include <linux/time.h>       // Using the clock to measure time between button presses
#include <linux/slab.h>
//#include <linux/export.h>

#define  DEBOUNCE_TIME 2    ///< The default bounce time -- 200ms
#define SAMPLING_SIZE 10
#define SCALING_FACTOR 100000000

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Marcin Jurczuk");
MODULE_DESCRIPTION("YF-S201 water flow sensor driver");
MODULE_VERSION("1.4");

static char *release = "1.4";

static bool sensorEdge = 0;                   ///< Falling Edge IRQ property
module_param(sensorEdge, bool, S_IRUGO);      ///WTF S_IRUGO generated a lot of errors !?. Which edge sensor genrates pulse
MODULE_PARM_DESC(sensorEdge, " Rising edge = 0 (default), Falling edge = 1"); 


// flow sensor models flow caltulations factors
// l/h -> tenths of flow sensor factor
// 5.5 -> 55
// 7.5 -> 75
static int sensor_ratio[] = { 75,55 };
static char* sensor_names[] = { "YF-S201", "FS300A" };
int sensorRatioValue = 55;

char *gpioSensor = "67:1";       /// Default sensor input GPIO is GPIO67
module_param(gpioSensor, charp, S_IRUGO);    
MODULE_PARM_DESC(gpioSensor, " GPIO PIN where pulse pin is connected(default=67) and name ex. 67:sensor1"); 

static int sensorModel = 1; // Support for different flow sensors
module_param(sensorModel, int, S_IRUGO);
MODULE_PARM_DESC(sensorModel, " Sensor model 1 = YF-201, 2 = FS300A");

static unsigned int samplingSize = 16;
module_param(samplingSize,int, S_IRUGO);
MODULE_PARM_DESC(samplingSize, " Number of samples to calculate average. Range 10-64, with 16 default");

char gpioName[32] = "1";      ///< Null terminated default string -- just in case
static unsigned int	  gpioNumber = 67;
// sensor name in parameters
//static char	  sensorName[32] = "1";

static int    irqNumber;                    ///< Used to share the IRQ number within this file
static int    pulseCounter = 0;            ///< For information, store the number of button presses
static int	  *periodSamples;
static int	  periodIndex = 0;

static struct timespec ts_last, ts_current, ts_diff;  ///< timespecs from linux/time.h (has nano precision)

/// IRQ handler declaration
static irq_handler_t  flowsensor_irq_handler(unsigned int irq, void *dev_id, struct pt_regs *regs);

/** @brief A callback function to output the processed pulses from sensor variable
 */
static ssize_t pulseCounter_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf){
   return sprintf(buf, "%d\n", pulseCounter);
}

/** @brief A callback function to read in current pulses processed by driver. Each pulse = 2.25 mL
 *  @param kobj represents a kernel object device that appears in the sysfs filesystem
 *  @param attr the pointer to the kobj_attribute struct
 *  @param buf the buffer from which to read the number of presses (e.g., reset to 0).
 *  @param count the number characters in the buffer
 *  @return return should return the total number of characters used from the buffer
 */

static ssize_t pulseCounter_store(struct kobject *kobj, struct kobj_attribute *attr,
                                   const char *buf, size_t count){
   sscanf(buf, "%du", &pulseCounter);
   return count;
}

/** @brief Show time when last reading was made from sensor. 
			  Usefull when you want to know when water stopped flowing
*/
static ssize_t lastTime_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf){
   return sprintf(buf, "%.2lu:%.2lu:%.2lu:%.9lu \n", (ts_last.tv_sec/3600)%24,
          (ts_last.tv_sec/60) % 60, ts_last.tv_sec % 60, ts_last.tv_nsec );
}
/** @brief - Current flow L/hour
*/
static ssize_t currentFlow_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf) {
	int i = 0;
	int averageFlow = 0;
	for (i = 0; i < SAMPLING_SIZE; i++) {
		averageFlow+=periodSamples[i];	
	}
	averageFlow = averageFlow/SAMPLING_SIZE;

	// flow calculation is :
		// YF-S201 - > frequency/7.5 = l/min 
		// FS300A -> frequency/ 5.5 = l/m
	// frequency * 8 = l/h

	//return sprintf(buf, "%.02d L/h\n", 60000000/(sensorRatioValue*averageFlow));
	return sprintf(buf, "%.02d L/h\n", 60000000/((averageFlow/10000)*sensorRatioValue));
	//return sprintf(buf, "%.02d L/h\n", ((averageFlow/SAMPLING_SIZE)/sensorRatioValue)*6000);
}
static ssize_t totalLiters_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf) {
	return sprintf(buf,"%d\n", pulseCounter/445);
}
static ssize_t pulseLength_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf){
   return sprintf(buf, "%lu.%.9lu\n", ts_diff.tv_sec, ts_diff.tv_nsec);
}

/** All files expordet to /sys for using by useland programs
 * Use these helper macros to define the name and access levels of the kobj_attributes
 *  The kobj_attribute has an attribute attr (name and mode), show and store function pointers
 *  The count variable is associated with the numberPresses variable and it is to be exposed
 */
static struct kobj_attribute count_attr = __ATTR(pulseCounter, 0444, pulseCounter_show, pulseCounter_store);
static struct kobj_attribute totall_attr  = __ATTR_RO(totalLiters);  ///< the last time pressed kobject attr
static struct kobj_attribute time_attr  = __ATTR_RO(lastTime);  ///< the last time pressed kobject attr
static struct kobj_attribute pulse_attr  = __ATTR(pulseLength,0444,pulseLength_show,NULL);  ///< the difference in time attr
static struct kobj_attribute flow_attr = __ATTR(currentFlow, 0444, currentFlow_show,NULL);

/**  flowsensor_attrs[] exports data to sysfs for userspace programs
 */
static struct attribute *flowsensor_attrs[] = {
		&totall_attr.attr,						//total liters flowed by sensor
      &count_attr.attr,                  ///< The number of pulses from sensor
      &time_attr.attr,                   ///< Time of the last pulse received by sensor HH:MM:SS:NNNNNNNNN
      &pulse_attr.attr,                   
		&flow_attr.attr,
      NULL,
};

static struct attribute_group attr_group = {
      .name  = gpioName,                 ///< The name that will be genrated by init function
      .attrs = flowsensor_attrs,                //Attr array
};

static struct kobject *flowsensor_kobj;

/** @brief The LKM initialization function
 *  The static keyword restricts the visibility of the function to within this C file. The __init
 *  macro means that for a built-in driver (not a LKM) the function is only used at initialization
 *  time and that it can be discarded and its memory freed up after that point. In this example this
 *  function sets up the GPIOs and the IRQ
 *  @return returns 0 if successful
 */
static int __init flowsensor_init(void){
	int result = 0;
	int arg_parse_result = 0;
	unsigned long IRQflags = IRQF_TRIGGER_FALLING;      // The default is a rising-edge interrupt

	printk(KERN_INFO "Initializing flowsensor driver v%s \n",release);
	if ( sensorModel > 0 && sensorModel < 3) {
		printk(KERN_INFO "Selected %s flow sensor with scale factor %d\n", sensor_names[sensorModel-1],sensor_ratio[sensorModel-1]);
	} else {
		printk("Unsuported flow sensor param provided ! Going back to default YF-201\n");
		sensorModel = 1;
	}
	sensorRatioValue = sensor_ratio[sensorModel-1];
	if ( samplingSize > 64 || samplingSize < 10 ) {
		printk("Using default sampling size (16)");
		samplingSize = 16;
	} else {
		printk("Using %u as sampline size\n", samplingSize);
	}
	periodSamples = kmalloc(samplingSize*sizeof(unsigned int),GFP_KERNEL);
	if (!periodSamples) {
		printk("Aaaa - kmalloc returned error. I don't know what to do..");
	}
	
	
	//sprintf(gpioName, "gpio%d", gpioSensor);           // Create the gpio115 name for /sys/flowsensor/gpio67
	arg_parse_result = sscanf(gpioSensor,"%u:%s", &gpioNumber,&gpioName);
	//	if (arg_parse_result < 1) {
	printk(KERN_ALERT "Using GPIO %d, and flowsensor name %s\n", gpioNumber, gpioName);
	//}
	flowsensor_kobj = kobject_create_and_add("flowsensor", kernel_kobj->parent); // kernel_kobj points to /sys/kernel
	if(!flowsensor_kobj){
		printk(KERN_ALERT "FlowSensor: failed to create kobject mapping\n");
		return -ENOMEM;
	}
   // add the attributes to /sys/flowsensor/
   result = sysfs_create_group(flowsensor_kobj, &attr_group);
   if(result) {
      printk(KERN_ALERT "FlowSensor: failed to create sysfs group\n");
      kobject_put(flowsensor_kobj);                          // clean up -- remove the kobject sysfs entry
      return result;
   }
   getnstimeofday(&ts_last);                          // set the last time to be the current time
   ts_diff = timespec_sub(ts_last, ts_last);          // set the initial time difference to be 0

   gpio_request(gpioNumber, "sysfs");       // Set up the sensor GPIO
   gpio_direction_input(gpioNumber);        // Set the button GPIO to be an input
   gpio_set_debounce(gpioNumber, DEBOUNCE_TIME); // Debounce pulses with defined delay
   gpio_export(gpioNumber, false);          // Causes gpioXX to disappear in /sys/class/gpio
			                    // the bool argument prevents the direction from being changed
	// Get assigned IRQ for our GPIO port
   irqNumber = gpio_to_irq(gpioNumber);
   printk(KERN_INFO "FlowSensor: Sensor is mapped to IRQ: %d\n", irqNumber);

   if(sensorEdge){                           // If the kernel parameter sensorEdge=1 is supplied
      IRQflags = IRQF_TRIGGER_RISING;      // Set the interrupt to be on the falling edge
   }
   // This next call requests an interrupt line
   result = request_irq(irqNumber,             // The interrupt number requested
                        (irq_handler_t) flowsensor_irq_handler, // The pointer to the handler function below
                        IRQflags,              // Use the custom kernel param to set interrupt type
                        "flowsensor_handler",  // Used in /proc/interrupts to identify the owner
                        NULL);                 // The *dev_id for shared interrupt lines, NULL is okay
   return result;
}

// Cleanup when module unload
static void __exit flowsensor_exit(void){
   kobject_put(flowsensor_kobj);                   // clean up -- remove the kobject sysfs entry
   free_irq(irqNumber, NULL);               // Free the IRQ number, no *dev_id required in this case
	kfree(periodSamples);	// release sampling array
   gpio_unexport(gpioNumber);               // Unexport the Button GPIO
   gpio_free(gpioNumber);                   // Free the Button GPIO
   printk(KERN_INFO "YF-S201 FlowSensor driver unloaded. Cleanup complete!\n");
}

/** @brief The GPIO IRQ Handler function
 *  This function is a custom interrupt handler that is attached to the GPIO above. The same interrupt
 *  handler cannot be invoked concurrently as the interrupt line is masked out until the function is complete.
 *  This function is static as it should not be invoked directly from outside of this file.
 *  @param irq    the IRQ number that is associated with the GPIO -- useful for logging.
 *  @param dev_id the *dev_id that is provided -- can be used to identify which device caused the interrupt
 *  Not used in this example as NULL is passed.
 *  @param regs   h/w specific register values -- only really ever used for debugging.
 *  return returns IRQ_HANDLED if successful -- should return IRQ_NONE otherwise.
 */
static irq_handler_t flowsensor_irq_handler(unsigned int irq, void *dev_id, struct pt_regs *regs){
   getnstimeofday(&ts_current);         // 
   ts_diff = timespec_sub(ts_current, ts_last);   // Determine the time difference between last 2 presses
   ts_last = ts_current;                // Store the current time as the last time ts_last
	if (periodIndex > samplingSize) {
		periodIndex = 0;	
	}
	periodSamples[periodIndex] = ts_diff.tv_nsec;
	periodIndex++;
	pulseCounter++;                     
   return (irq_handler_t) IRQ_HANDLED;  // Announce that the IRQ has been handled correctly
}

// This next calls are  mandatory -- they identify the initialization function
// and the cleanup function (as above).
module_init(flowsensor_init);
module_exit(flowsensor_exit);
