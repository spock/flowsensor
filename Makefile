obj-m+=flowsensor.o
KVER=/usr/src/linux-4.4.4

all:
	make ARCH=arm -C ${KVER} M=$(PWD) CROSS_COMPILE=arm-linux-gnueabi- modules
clean:
	make ARCH=arm -C ${KVER} M=$(PWD) CROSS_COMPILE=arm-linux-gnueabi- clean
